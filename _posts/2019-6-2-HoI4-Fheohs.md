---
layout: post
author: Robyn
category: fheohs
tags: [Hoi4, Git, Gitlab, Kuhy, Fheohs]
image: /assets/images/postImages/fheohs/2019/06/02/fheohs.jpg
---

This is a mod for Hearts of Iron 4 I have started to bring Fheohs into HoI4.
At the moment their is just the heigt map implemented and some work done on the terrain and provences.
The next task will be to create all the nessosery assets (flags ect).
[Gitlab Repo](https://gitlab.com/RobynHeib/Hoi4Fheohs)

<br>
<a target="_blank" href="{{ site.baseurl }}/assets/images/postImages/fheohs/2019/06/02/fheohs.jpg"><img src="{{ site.baseurl }}/assets/images/postImages/fheohs/2019/06/02/fheohs_thum.jpg" alt="Wip of the map of Kuhy" class="rounded img-fluid my-2"/></a>

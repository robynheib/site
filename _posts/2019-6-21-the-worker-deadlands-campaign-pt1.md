---
layout: post
author: Robyn
category: deadlands
tags: [RP, Vague, "Wild West", Deadlands]
---

Over the past year I have been playing a game of deadlands at Manchester Vague (my uni's role play society) where we managed to take a three month detour from the plot because one of our group members decided to drug the guys we were being escorted by (the GM literally had written in his notes "escorted to town nothing happens").

For the campaign I had decided to play as the main character from my comic 7by7 Coda (or Henry) to better understand the character. We started in Salt Lake City in Utah and where we were employed to test out a mechanical ghost rock power coach by travelling in it from Salt Lake City to Cedar City ( I think but can't 100% remember). The characters that joined Coda on this trip were: Lacy, a mad scientist; The Professor, an academic working for the agency; Patch, a kid; Al, a huckster and will a Trickster. We were told by the scientist that had made the coach that all we needed to do is press the button on the side of the coach to start it and press it again when we needed to stop. The coach would know what direction to go in. The coach had windows along either side and rows of seats. At the front of the coach was a metal figure holding reins to not spook people with a self driving coach. 

We set off on our journey and stopped at midday by pressing the button on the side of the coach. Coda and The Professor had forgotten to get any rations. Al boached a roll forcing her to give a ration to Coda to spite The Professor. Al then sold The Professor a ration for $20. In town this would have cost 50¢. After eating our rations we noticed, where's the kid...

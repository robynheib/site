---
layout: post
author: Robyn
category: Post-DemonCore
tags: [Doom, Programming, 7by7, Hjasn, Post-DemonCore, ps1]
image: https://img.itch.zone/aW1nLzg1NDIzODgucG5n/360x286%23c/3xCqkU.png
---
Change Log:
+ added dr_bonetower map as the first level
+ changed controls so that it controls simlar to a ps1 game
+ added controls menu
+ added audio menu
+ fixed issue with fog
+ added some of the puzzle elements to dr_forest

<iframe src="https://itch.io/embed/1219046?bg_color=000000&amp;fg_color=ffffff&amp;link_color=f79191" width="208" height="167" frameborder="0"><a href="https://theblackcorridor.itch.io/post-demoncore">Post-DemonCore by The Black Corridor</a></iframe>

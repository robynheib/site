---
layout: post
author: Robyn
category: Post-DemonCore
tags: [Doom, Programming, 7by7, Wolfenstein, Quake, Hjasn, Post-DemonCore]
image: /assets/images/post-demoncore/DevLogs/DevLog1/thum.png
---

Post-DemonCore started out as a Doom 2 mod. “Post-DemonCore Doom”. The Mod was set in my 7by7 universe where the PC decided to attack one of the Hjasn bases only to find they opened a portal to hell. The setting sounded pretty standard for Doom. Kind of a mixture between Wolfenstein and Doom with the Hjasn being facist.


![](/assets/images/post-demoncore/DevLogs/DevLog1/1.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/2.png)

The First map functioned like the first map in Quake. It allowed you to select the difficulty and level you wished to play. It also had a tutorial area. The white texture was converted to doom format which made this glitchy effect.


![](/assets/images/post-demoncore/DevLogs/DevLog1/3.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/4.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/5.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/6.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/7.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/8.png)

The First level was called “over run”. It was located in the Hjasn base where it had been over run by demonic creatures. The exit was a door in the middle of a lava pool that looks like it has nothing on the other side.

![](/assets/images/post-demoncore/DevLogs/DevLog1/9.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/10.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/11.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/12.png)

When the player passes through the door they end up in a different place in the base. They are further underground closer to where they have been digging for an anomaly. This level was the main reason I didn't finish the mod. On the 3rd picture for this map you can see a lift. This lift took ages to make and was really pushing the gzdoom engine. It would also periodically break if I touched it in any way.

![](/assets/images/post-demoncore/DevLogs/DevLog1/13.png)
![](/assets/images/post-demoncore/DevLogs/DevLog1/14.png)

At this point the map took a turn and had the player shooting the Hjasn military still present in the base. The player meets the character Lance. Lance helps the player operate the teleporter to try and get back to the surface and find a way off the planet. The scripting for Lance was a pain to make. And if he got attacked there was a chance that his path finding would mess up and you would have to restart the level. The teleporter then teleports the player to the surface but lance doesn't appear. Outside is chaos with soldiers fighting the demonic creatures.

<iframe src="https://itch.io/embed/1219046?bg_color=000000&amp;fg_color=ffffff&amp;link_color=f79191" width="208" height="167" frameborder="0"><a href="https://theblackcorridor.itch.io/post-demoncore">Post-DemonCore by The Black Corridor</a></iframe>

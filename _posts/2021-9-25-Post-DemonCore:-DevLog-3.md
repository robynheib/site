---
layout: post
author: Robyn
category: Post-DemonCore
tags: [Doom, Programming, 7by7, Hjasn, Post-DemonCore, ps1]
image: /assets/images/post-demoncore/DevLogs/DevLog3/thum.jpg
---
This week I have been working on a mix of designing monsters for the game and creating systems to build content on later.

## Change Log:
+Dev Console

+god mode

+map command

+Players now die when they fall off the world

+First level "Intro Dream"

+Settings Menu (not fully populated)

+Door messages


## Monsters
### Cthugha
Cthugha is the main monster in the hell dimension. They are a great old one. They also can be referred to as: The Living Flame, The Burning One or Kathigu-Ra

### Fthaggua's Frith (Fire Vampires)
Fthaggua is and entity created by Cthugha. They are the King Regent of the Frith. The Frith are electrical discharges of bright crimson and feast upon the life-energy of sentient creatures by burning them alive.

![Frith](/assets/images/post-demoncore/DevLogs/DevLog3/Frith.png)

### Fragth
The Fragth are based upon Baphomet, they were allegedly worshipped by the Knights Templar starting in the 1300s. The "Sabbatic Goat" depiction of Baphomet was originally drawn by Éliphas Lévi which contains binary elements representing the "symbolization of the equilibrium of opposites". They are inheritance of the hell dimension and are hostile because they feel their land is being invaded. 

![Fragth](/assets/images/post-demoncore/DevLogs/DevLog3/Fragth.png)

### Ktarfix
The Ktarfix are based of a Zdzisław Beksiński. They are a creature that grows around a crucifix. 

![Ktarfix](/assets/images/post-demoncore/DevLogs/DevLog3/Ktarfix.jpg)

### Ghast 
Ghasts are a humanoid creature but with the legs of a kangaroo and no forehead.

### Ghoul
Ghouls are Hjasn Solderer who have died and been reanimated. They do not rot. Their fat and muscles seem to shrink and some are fused with the walls. The look of these are based on some Zdzisław Beksiński paintings where a creatures skin is super tight and the bones are clearly visible through the skin.

### Rin
Rin are based of another Zdzisław Beksiński painting. They are humanoid figures but with no face. They are completely blind.

### Shinsha
Shinsha is also based of a Zdzisław Beksiński painting. They are a giant cloaked humanoid figure that lets out a harrowing distorted growl.

![Shintsha](/assets/images/post-demoncore/DevLogs/DevLog3/Shintsha.png)

### Yithatr

Yithatr, based of another Zdzisław Beksiński painting, is a giant cow like creature but is has more legs than it looks like is should. This one reminds me of an AT-AT from Star Wars.

![Yithatr](/assets/images/post-demoncore/DevLogs/DevLog3/Yithatr.png)

## Game Updates 

### Console
A big thing I wanted to add to the game was a developer console. This would make it easier to test certain features in the game without having to go through everything every time. The new map command also helps allot with this. The command allows you to type the name of a map and the game will load that map. Its functionality is the same as the map command in the Doom engine (and then later the Source engine after it was licensed to Valve). To load the test map the command would be ```map testmap```. The console is opened with the \` key (same as the default in Doom and Source).
The god mode is activated with the word god and makes it so the player cannot take any damage (same as in Doom and Source). 


### First level
The first level in true Lovecraft fashion is a dream sequence. The player gets told about a flame being stuck in someones mind. The level may be expanded of further but correctly consists of a single puzzle to introduce the player to how they play out.

### Setting
A system for having a settings menu has been implement. It currently consists of a full screen check box which can toggle full screen on and off. Later I will be adding functionality to change key bindings and and audio volumes. 

### Door massage system
If a door is locked and requires a key or is broken it will display this information on the screen when interacted with.

## Story
The story sets place in the milky way galaxy somewhere in Hjasn territory. The time is meant to feel like space age 1930's. In the time line it is just before the equivalent of the Spanish civil war kicks off.
The PC (player character) is a rebel from a group of people that are trying to take down the Hjasn empire. The rebel group know there is something important at a specific Hjasn base. But when the PC gets there to investigate and scout it out they find that the Hjasn have opened a portal to hell and they get trapped in there with all the Demons spilling into the base. The PC is issued a Rifle similar to the M96 Mauser.

<iframe src="https://itch.io/embed/1219046?bg_color=000000&amp;fg_color=ffffff&amp;link_color=f79191" width="208" height="167" frameborder="0"><a href="https://theblackcorridor.itch.io/post-demoncore">Post-DemonCore by The Black Corridor</a></iframe>

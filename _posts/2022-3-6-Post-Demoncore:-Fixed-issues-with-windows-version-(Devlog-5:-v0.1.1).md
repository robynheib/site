---
layout: post
author: Robyn
category: Post-DemonCore
tags: [Doom, Programming, 7by7, Hjasn, Post-DemonCore, ps1]
image: https://img.itch.zone/aW1nLzg1NDIzODgucG5n/360x286%23c/3xCqkU.png
---
Change log:
+Fixed issue where the collision mesh for the door outside the shop blocked the path
+Added start bone tower map
+Fixed missing meshes on windows


The main point of this update was to fixed problems with the windows version where the game couldn't read some of the object meshes. To fix this I added the mesh while on the windows version of Godot and compiled it from there.

A now map was also added at the start of the game. In this the player finds themselves in hell. In future updates the Ktarfx will attack the player. The music for this map was created using samples from old 90s abandon CDs I found on archive.org.

<iframe src="https://itch.io/embed/1219046?bg_color=000000&amp;fg_color=ffffff&amp;link_color=f79191" width="208" height="167" frameborder="0"><a href="https://theblackcorridor.itch.io/post-demoncore">Post-DemonCore by The Black Corridor</a></iframe>

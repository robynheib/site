---
layout: post
author: Robyn
category: Post-DemonCore
tags: [Doom, Programming, 7by7, Hjasn, Post-DemonCore, ps1]
image: https://img.itch.zone/aW1nLzgzMTU3NDgucG5n/original/YD4Giv.png
---
## Saving
Since The last Dev log The main thing I have managed to implement is the save system. This works by searching for "persist" nodes and running a "save" method on them. This method returns a JSON object with the variables that need to be persisted after a game load. These values are saved to a line in the save file where they can be read when the player loads a save. Loading works by deleting all the persist nodes then loading in a new instance of them and assigning the values specified on their save line. Currently there is only one save file but the plan is to save the file with the name of the save room they are currently in as well as the time the player has been playing.


<img src="https://img.itch.zone/aW1nLzgzMTU3NDgucG5n/original/YD4Giv.png">
## Levels
I have also managed to complete the first level which is now fully playable with functioning save mechanics. The last thing to do is to add character dialog. Hopefully this will give a bit more context about the PC.
<img src="https://img.itch.zone/aW1nLzgzMTU3NTMucG5n/original/LMZ7mt.png">
<img src="https://img.itch.zone/aW1nLzgzMTU3NjAucG5n/original/mzqymj.png">
<img src="https://img.itch.zone/aW1nLzgzMTU3NjUucG5n/original/NKK4DB.png">
<img src="https://img.itch.zone/aW1nLzgzMTU3NjgucG5n/original/2wQVio.png">
The forest section is also coming on well. I have added the forest as well as the save room which is a ww1 style bunker. I am thinking in this section to add some enemies but not give the player a gun to defend themselves. The player will have to avoid the enemies and kite them around. The enemies in the dream section are planed to be half dead enemies soldiers, stumbling around (more or less a zombie style enemies). The forest section uses a silent hill style fog to disguise the low draw distance. This also doubles as making the area seem bigger because the player cannot see the far side. In the forest section there is planned to be a cypher puzzle. Pre ww2 the British army mainly used a book cypher. This works by giving a page and word number to encrypt the message. For the game I wanted to have the hell section have heavy christian hell vibes, with a reflection of British imperialism. For the book cypher I have planned to use a bible cypher. The player is given a verse and a letter number for each character in a word combination lock. The bible chapter the cypher uses is Revelations giving those hell vibes. The player will have to search the forest to find the pieces of the cypher and decode them.
<img src="https://img.itch.zone/aW1nLzgzMTU3NzQucG5n/original/ZrWfOs.png">
<img src="https://img.itch.zone/aW1nLzgzMTU3NzcucG5n/original/ICx1pg.png">
<img src="https://img.itch.zone/aW1nLzgzMTU3ODEucG5n/original/meCPBa.png">
<img src="https://img.itch.zone/aW1nLzgzMTU4NDEuanBn/original/J83%2B53.jpg">
<img src="https://img.itch.zone/aW1nLzgzMTU4NDIuanBn/original/NsnInC.jpg">
The last section of the dream sequence is planned to be set within a hotel. At first them player must search the hotel for room number 7. Withing this room is a summoning circle. the player must then find 3 candles and a lighter to activate the circle. Screams are heard then the player is thrust into a hell version of the hotel where the the hole thing is burning. stepping outside of room seven there are now enemies roaming the halls. By this point the player has obtained a gun. This will be the Enfield no2 Mk1. A single action revolver used by the British army during ww1 and ww2. The player must try end leave the hotel. Once outside the hotel is the only thing around other than an endless plain of concrete. In front of the player is a bottomless pit. The player must jump down. At this point the player will wake up onboard the rebel ship and act 2 will start.</p>
<img src="https://img.itch.zone/aW1nLzgzMTU4MzcuanBn/original/OiYyKP.jpg">
<img src="https://img.itch.zone/aW1nLzgzMTU4MzguanBn/original/OYrQy6.jpg">
I am planning to release the game officially when act 1 is feature complete and in beta stage of development. I will fix up any major bugs before moving onto act 2.
#### FreeSounds used
<a href="https://freesound.org/people/LordStirling/sounds/483692/#" target="_blank">torch ambience loop</a><a href="https://freesound.org/people/LordStirling/sounds/483692/#" target="_blank"></a>
<a href="https://freesound.org/people/Jon285/sounds/49512/#" target="_blank">357Mag.wav</a>
<a href="https://freesound.org/people/acidsnowflake/sounds/402790/#" target="_blank">Cocking a revolver</a>
<a href="https://freesound.org/people/NachtmahrTV/sounds/556706/#" target="_blank">Scream in pain</a>
<a href="https://freesound.org/people/old_waveplay/packs/29763/" target="_blank">[Ambient Loop] Simple ambient loops</a>
<a href="https://freesound.org/people/old_waveplay/sounds/529137/#" target="_blank">[Ambient Loop] Deep ambient pulses 2</a>

<iframe src="https://itch.io/embed/1219046?bg_color=000000&amp;fg_color=ffffff&amp;link_color=f79191" width="208" height="167" frameborder="0"><a href="https://theblackcorridor.itch.io/post-demoncore">Post-DemonCore by The Black Corridor</a></iframe>

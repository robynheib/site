---
layout: post
author: Robyn
category: Post-DemonCore
tags: [Doom, Programming, 7by7, Hjasn, Post-DemonCore, ps1]
image: https://i.ytimg.com/vi_webp/e813JnHRUfg/sddefault.webp
---

I decided to pick up the project again after starting my silent hill inspired game. I needed to create something first that I could do in first person. So I decided to create the Doom mod I was making but in the Godot engine. This way I wouldn't be held back by the gzdoom engine. I plan to make the game using fully 3d models to keep the ps1 athletic from the silent hill game.

I first used most of the same code from my silent hill game but changed the camera to be first person. This allowed the player to move around with turn left and right and move forward and backwards. After this I added strafe controls which made the game like OG Doom. I then added mouse look and interactable objects. The interactable object gets sent a reference to the player. With this I was able to create a silent hill style door which teleports the player to the other side of it when interacted with. This also allowed me to create a health station like the ones in Half-life and another door that can switch the level. A global object called player was also created that contains things like the players health and ammo count. This sends out a signal when these values change which is used to update the display. The health display works by slowly turning the screen red as the player loses health. I wanted something that didnt tell the player exactly what health they're at but is able to gauge how badly they are hurt.

<div class="youtube">
<iframe class="embed" width="1280" height="720" src="https://www.youtube.com/embed/e813JnHRUfg" title="YouTube video player" frameborder="0" allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<iframe src="https://itch.io/embed/1219046?bg_color=000000&amp;fg_color=ffffff&amp;link_color=f79191" width="208" height="167" frameborder="0"><a href="https://theblackcorridor.itch.io/post-demoncore">Post-DemonCore by The Black Corridor</a></iframe>

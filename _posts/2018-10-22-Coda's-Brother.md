---
layout: post
author: Robyn
category: comic
image: /assets/images/postImages/comic/2018/10/22/JackWIP.jpg
tag: Comic
---

I have currently been focusing on the script for the first issue but managed to get the a WIP idea if how I want Coda's brother (Jack) to look.

<br>
<p><a href="{{ site.baseurl }}/assets/images/postImages/comic/2018/10/22/JackWIP.jpg"><img src="{{ site.baseurl }}/assets/images/postImages/comic/2018/10/22/JackWIP_thum.jpg" alt="Wip of Coda's Brother (Jack)" class="rounded img-fluid my-2"/></a></p>

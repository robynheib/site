---
layout: post
author: Robyn
category: deadlands
tags: [RP, Vague, "Wild West", Deadlands]
---

Lacy is a mad scientist proficient in gun graft. She had put together a rifle that has the damage of a cannon and no range modifier. When we found out the kid was missing Lacy stud on top of the coach and used her scope to find the kid as nobody on the ground could see him.
The kid was off in the distance wandering around. We all walk out to where Lacy saw the kid and just as we get to him. The ground caves in.

 Most of us taking some damage we stand up and dust ourselves off as a man come charging at us with an axe. With one man against 5 with guns. He's dead. With a closer look at him he was a native. The hole we had fallen in was pretty deep and would need to be climbed out of. 

We decided to investigate the caven we had fallen into. There was a smaller cave next to the big one with clothes in a pile in the corner and what looked like a person who had been eaten. We heard a screech from the far side of the bigger caven. At this point all the players had worked out what this means but we all failed our rolls for the characters to work this out. We went into the main caven and started to climb. The screeches started to get louder. Managing to get out we decided to just get back in the coach and not look back.

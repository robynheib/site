---
layout: post
author: Robyn
category: comic
image: /assets/images/postImages/comic/2019/06/21/Issue1Page1.png
tag: Comic
---

I have finally finished the scripted for issue one of 7 by 7 and have started work on drawing the pages. This is the first page.

<p><a target="_blank" href="{{ site.baseurl }}/assets/images/postImages/comic/2019/06/21/Issue1Page1.png"><img src="{{ site.baseurl }}/assets/images/postImages/comic/2019/06/21/Issue1Page1_thum.png" alt="Page1"     class="rounded img-fluid my-2"/></a></p>

<br>
<br>
But I may end up redoing them again.

Back cover blurb:

The general premise of 7by7 is that no one is inherently good or evil, just different situations that make people. There is no “good guy” or “bad guy” there is just people. Where they live and who they meet makes them who they are.
The story of 7by7 focuses on Henry Evans the son of the Master of the Universe. Henry disagrees with the way his dad is running his country. Being it a fascist imperial empire. With tension building up between them and rebel activity in the city rising the days ahead are turbulent but Henry is not the stories “good guy” he is just who the story is about.

And neither is his father the stories “bad guy”.
